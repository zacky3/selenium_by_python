# coding:utf-8
# チャットワークのテスト
from selenium import webdriver

# Firefoxを立ち上げて、チャットワークを開く
driver = webdriver.Firefox()
driver.get("https://www.chatwork.com")
driver.find_element_by_css_selector("#login_email").send_keys("自分のメールアドレス")
driver.find_element_by_name("password").send_keys("自分のパスワード")
driver.find_element_by_name("login").find_element_by_class_name("btnLarge").click()

# ページが読み込まれるまで、任意の時間待機する（表示されるまで待つ方法もありそう） 
driver.set_page_load_timeout(20)

# 特定のチャットを検索して、5回メッセージを送信する
driver.find_element_by_css_selector("li[aria-label='特定のアカウント名']").click()
for i in range(5):
	driver.find_element_by_css_selector("#_chatText").send_keys("Hello World!")
	driver.find_element_by_css_selector("#_sendButton").click()