# Selenium Introduction by Python

業務改善につながるSeleniumのPythonコードです。

## Features

- チャットワークを開いて、メッセージを送信するプログラム

## Requirement

- Selenium
- Python 3

## Usage

Pythonファイルをダウンロードして実行

```console
$ python hoge.py
```

## Installation

Seleniumをインストール

```console
$ sudo pip install selenium
```

## Author

[@hiraike32](https://twitter.com/b4b4r07)